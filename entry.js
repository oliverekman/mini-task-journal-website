

const eljEntrys = document.getElementById("jEntrys");

fetch('http://localhost:5000/v1/api/entries/')
  .then(response => {
    return response.json();
  })
  .then(data => {
    console.log(data);
    generateEntries(data);
  });

 function generateEntries(data) {
     data.forEach(x => {
         console.log(x);

            let elEntry = document.createElement('div');
      
            let elEntryTitle = document.createElement('h3');
            elEntryTitle.innerText = x.title;
      
            let elEntryContent = document.createElement('p');
            elEntryContent.innerText = x.content;

            let elEntryAuthor = document.createElement('i');
            elEntryAuthor.innerText = x.author + " " + "-" + " ";

            let elEntryCreated = document.createElement('i') 
            elEntryCreated.innerText = x.created_at;

            elEntry.appendChild(elEntryTitle);
            eljEntrys.appendChild(elEntryTitle);
            elEntry.appendChild(elEntryContent);
            eljEntrys.appendChild(elEntryContent);
            elEntry.appendChild(elEntryAuthor);
            eljEntrys.appendChild(elEntryAuthor);
            elEntry.appendChild(elEntryCreated);
            eljEntrys.appendChild(elEntryCreated);
            
        });
     
 } 
