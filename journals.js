const elJournalsLog = document.getElementById("JournalsLog");

fetch('http://localhost:5000/v1/api/journals/')
  .then(response => {
    return response.json();
  })
  .then(data => {
    console.log(data);
    generateJournal(data);
  });

 function generateJournal(data) {
     data.forEach(x => {
         console.log(x);

            let elJournal = document.createElement('div');
      
            let elJournalName = document.createElement('h5');
            elJournalName.innerText = x.name;
      
            let elCreated_at = document.createElement('p');
            elCreated_at.innerText = x.created_at;
      
            elJournal.appendChild(elJournalName);
            elJournalsLog.appendChild(elJournalName);
            elJournal.appendChild(elCreated_at);
            elJournalsLog.appendChild(elCreated_at);


        });
     
 } 
