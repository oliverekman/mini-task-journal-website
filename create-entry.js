


const elSelectAuthor = document.getElementById("selectAuthor");

fetch('http://localhost:5000/v1/api/author/')
  .then(response => {
    return response.json();
  })
  .then(data => {
    console.log(data);
    generateAuthor(data);
  });

 function generateAuthor(data) {
     data.forEach(x => {
         console.log(x);

            let elAuthor = document.createElement("Author");
      
            let elAuthorName = document.createElement("option");
            elAuthorName.innerText = x.author_id;

            elAuthor.appendChild(elAuthorName);
            elSelectAuthor.appendChild(elAuthorName);


        });
     
 } 


 const elPlaceholderAuthor = document.getElementById("placeholderAuthor");

fetch('http://localhost:5000/v1/api/author/')
  .then(response => {
    return response.json();
  })
  .then(data => {
    console.log(data);
    generatePlaceholderAuthor(data);
  });

 function generatePlaceholderAuthor(data) {
     data.forEach(x => {
         console.log(x);

            let elPHAuthor = document.createElement("ul");
      
            let elPlaceholderAuthorName = document.createElement("li");
            elPlaceholderAuthorName.innerText = x.author_id + " " + "-" + " " + x.first_name + " " + x.last_name;

            elPHAuthor.appendChild(elPlaceholderAuthorName);
            elPlaceholderAuthor.appendChild(elPlaceholderAuthorName);


        });
     
 } 





const elSelectJournal = document.getElementById("selectJournal");

fetch('http://localhost:5000/v1/api/journals/')
  .then(response => {
    return response.json();
  })
  .then(data => {
    console.log(data);
    generateJournals(data);
  });

 function generateJournals(data) {
     data.forEach(x => {
         console.log(x);

            let elJournal = document.createElement("Journal");
      
            let elJournalName = document.createElement("option");
            elJournalName.innerText = x.journal_id;
      

      
            elJournal.appendChild(elJournalName);
            elSelectJournal.appendChild(elJournalName);



        });
     
 } 



 const elPlaceholderJournal = document.getElementById("placeholderJournal");

fetch('http://localhost:5000/v1/api/journals/')
  .then(response => {
    return response.json();
  })
  .then(data => {
    console.log(data);
    generatePlaceholderJournals(data);
  });

 function generatePlaceholderJournals(data) {
     data.forEach(x => {
         console.log(x);

            let elPHJournal = document.createElement("ul");
      
            let elPlaceholderJournalName = document.createElement("li");
            elPlaceholderJournalName.innerText = x.journal_id + " " + "-" + " " + x.name;
      

      
            elPHJournal.appendChild(elPlaceholderJournalName);
            elPlaceholderJournal.appendChild(elPlaceholderJournalName);



        });
     
 } 





document.getElementById('dataPost').addEventListener('submit', dataPost);

   function dataPost(event){
            event.preventDefault();

             let author_id = document.getElementById('selectAuthor').value;
             let journal_id = document.getElementById('selectJournal').value;
             let title = document.getElementById('writeEntryTitle').value;
             let content = document.getElementById('writeEntry').value;

            const options = {
                method: 'POST',
                body: JSON.stringify({title:title,content:content,author_id:Number(author_id),journal_id:Number(journal_id)}),
                headers: {
                    'Content-Type': 'application/json'
                }
            }


          fetch('http://localhost:5000/v1/api/entries', options)
            .then(res => res.json())
            .then(data => console.log('Success:', data))
            .catch(error => console.error('Error:', error))
            

            

    };

